library(tidyverse)
customPalPrim <- c("#244F71","#0189B2","#39BDC7","#768A88","#69AB43")
customPalSec <- c("#387C2B","#516A51","#816B3B","#A5B498","#E7DAAC")
customPalAcc <- c("#B5D134","#9DCBCB","#603341","#FDB813","#ED1C24")

ggplot(data=chickwts,aes(x=feed,y=weight,fill=feed)) + geom_boxplot()

data=chickwts[!chickwts$feed=="sunflower",] %>%droplevels()

data2 <- chickwts %>% filter(feed == "sunflower" | feed=="casein" | 
                               feed=="horsebean") %>% droplevels()


ggplot(data=data2,aes(x=feed,y=weight,fill=as.integer(feed))) + geom_boxplot() +
  scale_fill_gradientn(name = "Feed",colors = customPalAcc,guide="legend",
                       labels=sort(unique(data2$feed)),
                       breaks=as.integer(sort(unique(data2$feed)))) + 
  ggtitle("Chick Weights") +
  theme(text=element_text(family="Calibri"), 
        plot.title = element_text(family="Century Gothic",face = "bold")) 



                       